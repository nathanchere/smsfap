# SmsFap (SMS Forwarder and Proxy)

For when you need to keep a phone number active to receive SMS (e.g. two-factor auth messages) but don't want to carry that
phone or SIM card around with you all the time.

This is only intended for my own use, but it wouldn't take much work to adapt it to your own needs or make it more generic overall. Hardware mentioned is only what I've used - there are plenty of drop-in alternatives.

## Features

Forwards SMS messages from your SIM card to:

* Telegram (via a custom Telegram bot)
* Dedicated SmsFap mobile app (via RabbitMQ)

## Components

### SmsFap

Raspberry Pi with Huawei E220 USB 3G dongle. Linux.

### Message Queue

RabbitMQ. A free account on cloudamqp.com will work fine.

* ping
* messages

### SmsFapMobile

Android application.

### Telegram bot

Set up TBC

## Usage

TBC

## Imaginary FAQ

* Q: why Telegram? Why not WhatsApp? Or Messenger? Or Instagram?
  A: because fuck Facebook.

## Planned Features

* Dashboard
  * notify when prepaid credit top-up is due
  * last ping
  * messages - inbox / outbox
  * send SMS
  * link to credit top-up
  * country
  * phone number #
  
  
